//
//  CustomButton.swift
//  Corr
//
//  Created by Marco Margarucci on 12/10/21.
//

import UIKit

final class CustomButton: UIButton {
    // MARK: - Properties
    
    // Border width
    var borderWidth: CGFloat = 10.0
    // Border color
    var borderColor: UIColor = .white
    
    // Title
    var title: String? {
        didSet {
            setTitle(title, for: .normal)
        }
    }
    
    // MARK: - Functions
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setupView(cornerRadius: 27.5)
    }
    
    // Setup view
    fileprivate func setupView(cornerRadius: CGFloat) {
        clipsToBounds = true
        layer.cornerRadius = cornerRadius
        layer.borderColor = borderColor.cgColor
        layer.borderWidth = borderWidth
        layer.shadowColor = UIColor.darkGray.cgColor
        layer.shadowOffset = CGSize(width: 1.5, height: 1.5)
        layer.shadowRadius = 5.0
        layer.shadowOpacity = 0.8
        layer.masksToBounds = false
    }
}
