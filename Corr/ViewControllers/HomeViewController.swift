//
//  HomeViewController.swift
//  Corr
//
//  Created by Marco Margarucci on 12/10/21.
//

import UIKit
import MapKit
import SwiftUI

class HomeViewController: BaseViewController {

    // MARK: - Properties
    
    // View controller title
    fileprivate let viewControllerTitle: String = "RUN"
    
    // Start button
    private lazy var startButton: CustomButton = {
        let view = CustomButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.borderWidth = 0.0
        view.borderColor = .clear
        view.backgroundColor = .startColor
        view.title = "START"
        view.titleLabel?.font = UIFont(name: "AvenirNext-DemiBold", size: 16)!
        view.addTarget(self, action: #selector(startRunningSession), for: .touchUpInside)
        return view
    }()
    
    // Title label
    private lazy var titleLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.text = viewControllerTitle
        view.textColor = .brandPrimary
        view.font = UIFont(name: "AvenirNext-DemiBold", size: 20)
        return view
    }()
    
    // Map view
    private lazy var mapView: MKMapView = {
        let view = MKMapView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.alpha = 1
        view.tintColor = .white
        view.delegate = self
        return view
    }()
    
    // Location manager
    private var locationManager = LocationManager()
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupConstraints()
    }
    
    // MARK: - Functions
    
    // Start recording running session
    @objc private func startRunningSession() {
        let activeRunViewController = ActiveRunViewController()
        activeRunViewController.modalPresentationStyle = .fullScreen
        present(activeRunViewController, animated: true, completion: nil)
    }
    
    // Setup user interface
    fileprivate func setupUI() {
        locationManager.checkLocationAuthorization()
        view.addSubview(titleLabel)
        view.addSubview(mapView)
        view.addSubview(startButton)
    }
    
    // Setup view constraints
    fileprivate func setupConstraints() {
        // Title label constrainsts
        NSLayoutConstraint.activate([
            titleLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            titleLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            titleLabel.heightAnchor.constraint(equalToConstant: 50)
        ])
        // Map view constraints
        NSLayoutConstraint.activate([
            mapView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            mapView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            mapView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 10),
            mapView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        ])
        // Start button constraints
        NSLayoutConstraint.activate([
            startButton.widthAnchor.constraint(equalToConstant: 100),
            startButton.heightAnchor.constraint(equalToConstant: 55),
            startButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -15),
            startButton.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
    }
}

// MARK: - Extensions

extension HomeViewController: MKMapViewDelegate {
    func mapViewDidFinishLoadingMap(_ mapView: MKMapView) {
        mapView.showsUserLocation = true
        mapView.userTrackingMode = .follow
    }
}
