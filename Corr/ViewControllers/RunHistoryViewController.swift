//
//  RunHistoryViewController.swift
//  Corr
//
//  Created by Marco Margarucci on 17/10/21.
//

import Foundation
import UIKit

class RunHistoryViewController: BaseViewController {
    // MARK: - Properties
    
    // View controller title
    fileprivate let viewControllerTitle: String = "HISTORY"
    
    // Reusable ID
    private static let cellReuseID: String = "CELLREUSEID"
    
    // Title label
    private lazy var titleLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.text = viewControllerTitle
        view.textAlignment = .center
        view.textColor = .brandPrimary
        view.font = UIFont(name: "AvenirNext-DemiBold", size: 20)
        return view
    }()
    
    // Table view
    private lazy var tableView: UITableView = {
        let view = UITableView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.delegate = self
        view.dataSource = self
        view.register(RunHistoryTableViewCell.self, forCellReuseIdentifier: Self.cellReuseID)
        view.backgroundColor = .clear
        view.showsVerticalScrollIndicator = false
        view.separatorColor = .clear
        return view
    }()
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupConstraints()
    }
    
    // Setup user interface
    fileprivate func setupUI() {
        view.addSubview(titleLabel)
        view.addSubview(tableView)
    }
    
    // Setup view constraints
    fileprivate func setupConstraints() {
        // Title label constrainsts
        NSLayoutConstraint.activate([
            titleLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            titleLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            titleLabel.heightAnchor.constraint(equalToConstant: 50)
        ])
        // Table view constraints
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        ])
    }
}

// MARK: - Extensions

extension RunHistoryViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Run.fetchAll()?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Self.cellReuseID) as? RunHistoryTableViewCell, let run = Run.fetchAll()?[indexPath.row] else { return UITableViewCell() }
        // Configure cell
        cell.configureCell(run: run)
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let run = Run.fetchAll()?[indexPath.row] else { return }
        debugPrint(run.distance.toString(places: 2))
    }
}
