//
//  ActiveRunViewController.swift
//  Corr
//
//  Created by Marco Margarucci on 12/10/21.
//

import UIKit
import CoreLocation
import RealmSwift

class ActiveRunViewController: BaseViewController {
    // MARK: - Properties

    // Title font size
    private static let titleFontSize: CGFloat = 20
    // Subtitle font size
    private static let subtitleFontSize: CGFloat = 20
    // Start location
    private var startLocation: CLLocation!
    // End location
    private var endLocation: CLLocation!
    // Distance
    private var distance: Double = 0.0
    // Time
    private var time: Int = 0
    // Pace
    private var pace: Double = 0.0
    // Location
    private var coordinateLocations = List<Location>()
    // Location manager
    private var locationManager = LocationManager()
    // Timer
    private var timer = Timer()
    
    // Rounded stop button
    private lazy var stopButton: CustomButton = {
        let view = CustomButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.borderWidth = 0.0
        view.borderColor = .clear
        view.backgroundColor = .stopColor
        view.title = "STOP"
        view.titleLabel?.font = UIFont(name: "AvenirNext-DemiBold", size: 16)!
        view.addTarget(self, action: #selector(stop), for: .touchUpInside)
        return view
    }()
    
    // Title label
    private lazy var titleLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.text = "Running"
        view.font = UIFont(name: "AvenirNext-DemiBold", size: 20)
        view.textAlignment = .center
        view.font = view.font.withSize(Self.titleFontSize)
        view.textColor = .brandPrimary
        return view
    }()
    
    // Time label
    private lazy var timeLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.text = "00:00:00"
        view.font = UIFont(name: "AvenirNext-DemiBold", size: 16)
        view.textColor = .startColor
        return view
    }()
    
    // Pace title label
    private lazy var paceTitleLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.textColor = .brandPrimary
        view.text = "Average pace"
        view.font = UIFont(name: "AvenirNext-DemiBold", size: 18)
        return view
    }()
    
    // Pace subtitle label
    private lazy var paceSubtitleLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.textColor = .startColor
        view.text = "/ms"
        view.font = UIFont(name: "AvenirNext-DemiBold", size: 16)
        return view
    }()
    
    // Pace label
    private lazy var paceLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.textColor = .startColor
        view.text = "0:00"
        view.font = UIFont(name: "AvenirNext-DemiBold", size: 16)
        return view
    }()
    
    // Pace information stack view
    private lazy var packeInformationStackView: UIStackView = {
        let view = UIStackView(arrangedSubviews: [paceTitleLabel, paceLabel, paceSubtitleLabel])
        view.translatesAutoresizingMaskIntoConstraints = false
        view.alignment = .center
        view.axis = .vertical
        view.distribution = .equalSpacing
        return view
    }()
    
    // Distance title label
    private lazy var distanceTitleLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.textColor = .brandPrimary
        view.text = "Distance"
        view.font = UIFont(name: "AvenirNext-DemiBold", size: 18)
        return view
    }()
    
    // Distance label
    private lazy var distanceLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.textColor = .startColor
        view.text = "0.0"
        view.font = UIFont(name: "AvenirNext-DemiBold", size: 16)
        return view
    }()
    
    // Distance subtitle label
    private lazy var distanceSubtitleLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.textColor = .startColor
        view.text = "m"
        view.font = UIFont(name: "AvenirNext-DemiBold", size: 16)
        return view
    }()
    
    // Distance information stack view
    private lazy var distanceInformationStackView: UIStackView = {
        let view = UIStackView(arrangedSubviews: [distanceTitleLabel, distanceLabel, distanceSubtitleLabel])
        view.translatesAutoresizingMaskIntoConstraints = false
        view.alignment = .center
        view.axis = .vertical
        view.distribution = .equalSpacing
        return view
    }()
    
    // Main stack view
    private lazy var mainStackView: UIStackView = {
        let view = UIStackView(arrangedSubviews: [timeLabel, packeInformationStackView, distanceInformationStackView])
        view.translatesAutoresizingMaskIntoConstraints = false
        view.alignment = .center
        view.axis = .vertical
        view.distribution = .equalCentering
        return view
    }()
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupConstraints()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        locationManager.manager.delegate = self
        startRunningSession()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        stopRunningSession()
    }
    
    // MARK: - Functions
    
    @objc fileprivate func stop() {
        // Add the location to Realm database
        Run.addRunToDatabase(pace: pace, distance: distance, duration: time, locations: coordinateLocations)
        stopRunningSession()
        dismiss(animated: true, completion: nil)
    }
    
    // Setup user interface
    fileprivate func setupUI() {
        view.addSubview(titleLabel)
        view.addSubview(mainStackView)
        view.addSubview(stopButton)
    }
    
    // Setup view constraints
    fileprivate func setupConstraints() {
        // Title label constrainsts
        NSLayoutConstraint.activate([
            titleLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            titleLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            titleLabel.heightAnchor.constraint(equalToConstant: 50)
        ])
        // Main stack view constraints
        NSLayoutConstraint.activate([
            mainStackView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            mainStackView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            mainStackView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 8)
        ])
        // Stop button constraints
        NSLayoutConstraint.activate([
            stopButton.widthAnchor.constraint(equalToConstant: 100),
            stopButton.heightAnchor.constraint(equalToConstant: 55),
            stopButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -15),
            stopButton.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
    }
    
    // Start running sessions
    fileprivate func startRunningSession() {
        locationManager.manager.startUpdatingLocation()
        startTimer()
    }
    
    // End running session
    fileprivate func stopRunningSession() {
        locationManager.manager.stopUpdatingLocation()
        stopTimer()
    }
    
    // Start timer
    fileprivate func startTimer() {
        timeLabel.text = time.toString()
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
    }
    
    // Stop timer
    fileprivate func stopTimer() {
        timer.invalidate()
        time = 0
    }
    
    // Update timer
    @objc fileprivate func updateTimer() {
        time += 1
        timeLabel.text = time.toString()
    }
    
    // Compute pace
    private func computePace(time seconds: Int, miles: Double) -> String {
        pace = (Double(seconds)/miles.rounded(toPlaces: 2))
        return pace.toString(places: 2)
    }
}

// MARK: - Extensions

extension ActiveRunViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if startLocation == nil { startLocation = locations.first }
        else if let location = locations.last {
            distance += endLocation.distance(from: location)
            // Adding the locations to the list of locations
            let newLocation = Location(latitude: Double(endLocation.coordinate.longitude), longitude: Double(endLocation.coordinate.latitude))
            coordinateLocations.insert(newLocation, at: 0)
            self.distanceLabel.text = self.distance.getDistanceInMeters().toString(places: 2)
            if time > 0 && distance > 0.0 {
                paceLabel.text = computePace(time: time, miles: distance)
            }
        }
        endLocation = locations.last
    }
}
