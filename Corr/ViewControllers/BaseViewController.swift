//
//  BaseViewController.swift
//  Corr
//
//  Created by Marco Margarucci on 12/10/21.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .backgroundPrimary
    }
}
