//
//  Date+Extensions.swift
//  Corr
//
//  Created by Marco Margarucci on 17/10/21.
//

import Foundation

extension Date {
    func getDateString() -> String {
        let calendar = Calendar.current
        let year = calendar.component(.year, from: self)
        let month = calendar.component(.month, from: self)
        let day = calendar.component(.day, from: self)
        let dateString = "\(day)-\(month)-\(year)"
        return dateString
    }
}
