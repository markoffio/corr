//
//  UIColor+Extensions.swift
//  Corr
//
//  Created by Marco Margarucci on 10/10/21.
//

import Foundation
import UIKit

// Colors
extension UIColor {
    // Brand primary
    static let brandPrimary = UIColor(named: "brandPrimary")
    // Start color
    static let startColor = UIColor(named: "startColor")
    // Stop color
    static let stopColor = UIColor(named: "stopColor")
    // Background primary
    static let backgroundPrimary = UIColor(named: "backgroundPrimary")
}
