//
//  Double+Extensions.swift
//  Corr
//
//  Created by Marco Margarucci on 14/10/21.
//

import Foundation

extension Double {
    func getDistanceInMeters() -> Double {
        let meters = Measurement(value: self, unit: UnitLength.meters)
        return meters.converted(to: .miles).value
    }
    
    func toString(places: Int) -> String {
        return String(format: "%.\(places)f", self)
    }
    
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
