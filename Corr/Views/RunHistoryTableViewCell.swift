//
//  RunHistoryTableViewCell.swift
//  Corr
//
//  Created by Marco Margarucci on 17/10/21.
//

import UIKit

class RunHistoryTableViewCell: UITableViewCell {
    // MARK: - Properties
    
    // Total distance
    var totalDistance: Double = 0.0 {
        didSet {
            totalDistanceLabel.text = String(format: "%0.1f", totalDistance)
        }
    }
    
    // Total time
    var totalTime: String = "00:10:14" {
        didSet {
            totalTimeLabel.text = totalTime
            layoutIfNeeded()
        }
    }
    
    // Date
    var date: String = "14/07/2021" {
        didSet {
            dateLabel.text = date
            layoutIfNeeded()
        }
    }
    
    // Total distance label
    private lazy var totalDistanceLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.text = "0.0"
        view.textColor = .darkGray
        view.font = UIFont(name: "AvenirNext-DemiBold", size: 16)
        return view
    }()
    
    // Total time label
    private lazy var totalTimeLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.text = "0.0"
        view.textColor = .darkGray
        view.font = UIFont(name: "AvenirNext-DemiBold", size: 16)
        return view
    }()
    
    // Date label
    private lazy var dateLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.text = "0.0"
        view.textColor = .darkGray
        view.font = UIFont(name: "AvenirNext-DemiBold", size: 16)
        return view
    }()
    
    // MARK: - Functions
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // Setup user interface
    fileprivate func setupUI() {
        backgroundColor = UIColor.white
        contentView.addSubview(totalDistanceLabel)
        contentView.addSubview(totalTimeLabel)
        contentView.addSubview(dateLabel)
    }
    
    // Setup view constraints
    fileprivate func setupConstraints() {
        // Total distance label
        NSLayoutConstraint.activate([
            totalDistanceLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            totalDistanceLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 16)
        ])
        // Total time label
        NSLayoutConstraint.activate([
            totalTimeLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            totalTimeLabel.topAnchor.constraint(equalTo: totalDistanceLabel.bottomAnchor, constant: 8),
            totalTimeLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -16)
        ])
        // Date label
        NSLayoutConstraint.activate([
            dateLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
            dateLabel.centerYAnchor.constraint(equalTo: totalDistanceLabel.centerYAnchor)
        ])
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setupConstraints()
    }
    
    // Configure cell
    func configureCell(run: Run) {
        totalDistance = run.distance
        totalTime = run.duration.toString()
        date = run.date.getDateString()
    }
}
