//
//  TabBarViewController.swift
//  Corr
//
//  Created by Marco Margarucci on 12/10/21.
//

import UIKit

final class TabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        setupViewController()
    }
    
    // MARK: - Functions
    
    // Setup UI
    fileprivate func setupUI() { tabBar.tintColor = UIColor.brandPrimary }
    
    // Setup view controllers
    fileprivate func setupViewController() {
        viewControllers = [
            createViewController(for: HomeViewController(), title: "Run", imageName: "figure.walk"),
            createViewController(for: RunHistoryViewController(), title: "History", imageName: "list.bullet")
        ]
    }
    
    // Create view controllers
    fileprivate func createViewController(for viewController: UIViewController, title: String, imageName: String) -> UIViewController {
        // Icon used when the view controller is not selected
        let icon = UIImage(systemName: imageName)
        // Icon used when the view controller is selected
        let selectedIcon = UIImage(systemName: imageName, withConfiguration: UIImage.SymbolConfiguration(weight: .bold))
        // Tab bar item
        let tabBarItem = UITabBarItem(title: title, image: icon, selectedImage: selectedIcon)
        viewController.tabBarItem = tabBarItem
        return viewController
    }
}
