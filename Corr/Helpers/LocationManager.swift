//
//  LocationManager.swift
//  Corr
//
//  Created by Marco Margarucci on 12/10/21.
//

import Foundation
import CoreLocation

final class LocationManager {
    // MARK: - Properties
    
    var manager: CLLocationManager
    
    init() {
        manager = CLLocationManager()
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.activityType = .fitness
    }
    
    // MARK: - Functions
    
    func checkLocationAuthorization() {
        if manager.authorizationStatus != .authorizedWhenInUse { manager.requestWhenInUseAuthorization() }
    }
}
