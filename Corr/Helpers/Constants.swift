//
//  Constants.swift
//  Run
//
//  Created by Marco Margarucci on 17/10/21.
//

import Foundation

enum RealmQueue {
    static let realmQueue = DispatchQueue(label: "RealmQueue")
}

enum RunRealmProperties {
    // id
    static let id: String = "id"
    // Pace
    static let pace: String = "pace"
    // Date
    static let date: String = "date"
    // Duration
    static let duration: String = "duration"
}
