//
//  Location.swift
//  Corr
//
//  Created by Marco Margarucci on 17/10/21.
//

import Foundation
import RealmSwift

final class Location: Object {
    // Latitude
    @objc dynamic public private(set) var latitude = 0.0
    // Longitude
    @objc dynamic public private(set) var longitude = 0.0
    
    convenience init(latitude: Double, longitude: Double) {
        self.init()
        self.latitude = latitude
        self.longitude = longitude
    }
}
