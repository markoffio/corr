//
//  Run.swift
//  Corr
//
//  Created by Marco Margarucci on 17/10/21.
//

import Foundation
import RealmSwift

final class Run: Object {
    // Run identifier
    @objc dynamic public private(set) var id = UUID().uuidString.lowercased()
    // Pace
    @objc dynamic public private(set) var pace: Double = 0.0
    // Distance
    @objc dynamic public private(set) var distance: Double = 0.0
    // Duration
    @objc dynamic public private(set) var duration: Int = 0
    // Date
    @objc dynamic public private(set) var date = Date()
    // Location
    public private(set) var locations = List<Location>()
    
    // MARK: - Functions
    
    override class func primaryKey() -> String? {
        return RunRealmProperties.id
    }
    
    override class func indexedProperties() -> [String] {
        return [RunRealmProperties.date, RunRealmProperties.duration]
    }
    
    convenience init(pace: Double, distance: Double, duration: Int, locations: List<Location>) {
        self.init()
        self.date = Date()
        self.pace = pace
        self.distance = distance
        self.duration = duration
        self.locations = locations
    }
    
    // Add to Realm database
    static func addRunToDatabase(pace: Double, distance: Double, duration: Int, locations: List<Location>) {
        RealmQueue.realmQueue.sync {
            do {
                let realm = try Realm()
                try realm.write({
                    realm.add(Run(pace: pace, distance: distance, duration: duration, locations: locations))
                    try realm.commitWrite()
                })
            } catch (let error) {
                debugPrint("[ERROR]: Error adding entry to Realm database -> \(error.localizedDescription)")
            }
        }
    }
    
    // Fetch all elements from the database
    static func fetchAll() -> Results<Run>? {
        do {
            let realm = try Realm()
            var collection = realm.objects(Run.self)
            collection = collection.sorted(byKeyPath: RunRealmProperties.date, ascending: true)
            return collection
        } catch {
            return nil
        }
    }
}
